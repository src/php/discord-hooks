<?php

declare(strict_types=1);
namespace Adawolfa\DiscordHooks\Embed;
use Nette\SmartObject;
use JsonSerializable;
use Adawolfa\DiscordHooks\Serializer;

/**
 * Embed image.
 *
 * @property string $url
 */
final class Image implements JsonSerializable
{

	use SmartObject;
	use Serializer;

	/** @var string */
	private $url;

	public function __construct(string $url)
	{
		$this->setUrl($url);
	}

	public function getUrl(): string
	{
		return $this->url;
	}

	public function setUrl(string $url): void
	{
		$this->url = $url;
	}

}