<?php

declare(strict_types=1);
namespace Adawolfa\DiscordHooks;
use DateTimeInterface;

trait Serializer
{

	public function jsonSerialize()
	{
		$values = [];

		foreach ($this as $p => $v) {

			if ($v === null || $v === []) {
				continue;
			} elseif ($v instanceof DateTimeInterface) {
				$v = $v->format('Y-m-d\TH:i:s.v\Z');
			}

			$p = strtolower(preg_replace('~([A-Z])~', '_$1', $p));

			$values[$p] = $v;

		}

		return $values;
	}

}