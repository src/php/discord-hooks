<?php

declare(strict_types=1);
namespace Adawolfa\DiscordHooks;
use Nette\Utils\Strings;

/**
 * Announces project version after pull.
 */
final class VersionAnnouncer
{

	/** @var string */
	private $url;

	/** @var callable|null */
	private $decorator;

	/** @var string */
	private $root;

	/**
	 * Version announcer constructor.
	 * @param string $url
	 * @param callable|null $decorator
	 * @param string|null $root
	 */
	public function __construct(string $url, callable $decorator = null, string $root = null)
	{
		$this->url = $url;
		$this->decorator = $decorator;
		$this->root = $root === null ? getcwd() : realpath($root);
	}

	/**
	 * Announces a release.
	 */
	public function announce(): void
	{
		$old = @file_get_contents($this->root . '/.version');

		if (!$old) {
			$old = null;
		} else {
			$old = trim($old);
		}

		$new = trim(`git rev-parse HEAD`);

		if ($old === $new) {
			return;
		}

		$log = trim(`git show --format=fuller --quiet $new`);

		if (!preg_match('~commit (?<sha>[a-f0-9]{40})\nAuthor:\s*(?<author>[^\n]+)\nAuthorDate:\s*(?<author_date>[^\n]+)\nCommit:\s*(?<committer>[^\n]+)\nCommitDate:\s*(?<commit_date>[^\n]+)\n(?<message>.*)~s', $log, $matches)) {
			return;
		}

		$m = new Message;
		$m->embeds[] = $embed = new Embed;

		$message = trim(preg_replace('~(^|\n)[ \t]+~', "\n", $matches['message']));
		$message = Strings::normalizeNewLines($message);

		if (($pos = strpos($message, "\n")) !== false) {
			$title = substr($message, 0, $pos);
			$message = trim(substr($message, $pos + 1));
		} else {
			$title = $message;
			$message = null;
		}

		$embed->title = $title;
		$embed->description = $message;

		if ($this->decorator !== null) {
			call_user_func($this->decorator, $m, $matches);
		}

		$webHook = new WebHook($this->url);
		$webHook->execute($m);

		file_put_contents($this->root . '/.version', $new);
	}

}