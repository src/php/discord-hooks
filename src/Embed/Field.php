<?php

declare(strict_types=1);
namespace Adawolfa\DiscordHooks\Embed;
use Nette\SmartObject;
use JsonSerializable;
use Adawolfa\DiscordHooks\Serializer;

/**
 * Embed field.
 *
 * @property string $name
 * @property string $value
 * @property bool $inline
 */
final class Field implements JsonSerializable
{

	use SmartObject;
	use Serializer;

	/** @var string */
	private $name, $value;

	/** @var bool */
	private $inline;

	public function __construct(string $name, string $value, $inline = null)
	{
		$this->setName($name);
		$this->setValue($value);
		$this->setInline($inline);
	}

	public function getName(): string
	{
		return $this->name;
	}

	public function getValue(): string
	{
		return $this->value;
	}

	public function isInline(): bool
	{
		return $this->inline === true;
	}

	public function setName(string $name): void
	{
		$this->name = $name;
	}

	public function setValue(string $value): void
	{
		$this->value = $value;
	}

	public function setInline(?bool $inline): void
	{
		$this->inline = $inline;
	}

}