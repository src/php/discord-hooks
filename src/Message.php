<?php

declare(strict_types=1);
namespace Adawolfa\DiscordHooks;
use Nette\SmartObject;
use JsonSerializable;

/**
 * Discord message.
 *
 * @property string $content
 * @property string $username
 * @property string $avatarUrl
 * @property Embed[] $embeds
 */
final class Message implements JsonSerializable
{

	use SmartObject;
	use Serializer;

	/** @var string */
	private $content, $username, $avatarUrl;

	/** @var Embed[] */
	private $embeds = [];

	public function getContent(): ?string
	{
		return $this->content;
	}

	public function getUsername(): ?string
	{
		return $this->username;
	}

	public function getAvatarUrl(): ?string
	{
		return $this->avatarUrl;
	}

	public function &getEmbeds(): array
	{
		return $this->embeds;
	}

	public function setContent(?string $content): void
	{
		$this->content = $content;
	}

	public function setUsername(?string $username): void
	{
		$this->username = $username;
	}

	public function setAvatarUrl(?string $avatarUrl): void
	{
		$this->avatarUrl = $avatarUrl;
	}

}