<?php

declare(strict_types=1);
use Adawolfa\DiscordHooks;
use Nette\Utils\Json;

require(__DIR__ . '/../bootstrap.php');

$message = new DiscordHooks\Message;
$message->content = 'content';

$embed = new DiscordHooks\Embed;
$embed->timestamp = new DateTime('2019-12-13 05:30:00');
$embed->author = new DiscordHooks\Embed\Author('adawolfa');
$embed->author->iconUrl = 'icon url';
$message->embeds[] = $embed;

Tester\Assert::same([
	'content' => 'content',
	'embeds' => [
		[
			'timestamp' => '2019-12-13T05:30:00.000Z',
			'author' => [
				'name' => 'adawolfa',
				'icon_url' => 'icon url',
			],
		]
	],
], Json::decode(Json::encode($message), Json::FORCE_ARRAY));