<?php

declare(strict_types=1);
namespace Adawolfa\DiscordHooks\Tracy;
use Adawolfa\DiscordHooks;
use Throwable;
use Tracy;
use Nette\Utils\Strings;

/**
 * Tracy logger with Discord interception.
 */
final class Logger extends Tracy\Logger
{

	/** @var string */
	private $url;

	/** @var callable */
	private $decorator;

	public function __construct(string $url, ?callable $decorator, ?string $directory, $email = null, Tracy\BlueScreen $blueScreen = null)
	{
		parent::__construct($directory, $email, $blueScreen);
		$this->url = $url;
		$this->decorator = $decorator;
	}

	public function log($value, $level = self::INFO)
	{
		$file = parent::log($value, $level);

		if ($file && $value instanceof Throwable) {

			$sent = @file_get_contents($this->directory . '/discord-sent') ?: '';

			if (strpos($sent, $file) === false) {

				file_put_contents($this->directory . '/discord-sent', $file . "\n", FILE_APPEND | LOCK_EX);

				$m = new DiscordHooks\Message;
				$m->embeds[] = $embed = new DiscordHooks\Embed;

				$embed->title = get_class($value);
				$embed->description = Strings::truncate(Tracy\Logger::formatMessage($value), 2000);

				if (strpos($embed->description . ': ', $embed->title) === 0) {
					$embed->description = substr($embed->description, strlen($embed->title) + 2);
				}

				$embed->color = 15158332;

				$embed->footer = new DiscordHooks\Embed\Footer;

				if (php_sapi_name() !== 'cli' && php_sapi_name() !== 'cli-server') {
					$embed->footer->text = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 'https' : 'http') . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
				}

				if ($this->decorator !== null) {
					if (call_user_func($this->decorator, $m, $value) === false) {
						return $file;
					}
				}

				$hook = new DiscordHooks\WebHook($this->url);
				$hook->execute($m);

			}

		}

		return $file;
	}

	public static function setup(string $url, callable $decorator = null): void
	{
		$logger = new self($url, $decorator, Tracy\Debugger::$logDirectory, Tracy\Debugger::$email, Tracy\Debugger::getBlueScreen());
		$logger->directory = &Tracy\Debugger::$logDirectory;
		$logger->email = &Tracy\Debugger::$email;
		Tracy\Debugger::setLogger($logger);
	}

}