<?php

declare(strict_types=1);
namespace Adawolfa\DiscordHooks;
use Nette\SmartObject;
use DateTimeInterface;
use JsonSerializable;

/**
 * Discord embed.
 *
 * @property string $title
 * @property string $description
 * @property string $url
 * @property int $color
 * @property DateTimeInterface $timestamp
 * @property Embed\Footer $footer
 * @property Embed\Image $thumbnail
 * @property Embed\Image $image
 * @property Embed\Author $author
 * @property Embed\Field[] $fields
 */
final class Embed implements JsonSerializable
{

	use SmartObject;
	use Serializer;

	/** @var string */
	private $title, $description, $url;

	/** @var int */
	private $color;

	/** @var DateTimeInterface */
	private $timestamp;

	/** @var Embed\Footer */
	private $footer;

	/** @var Embed\Image */
	private $thumbnail;

	/** @var Embed\Image */
	private $image;

	/** @var Embed\Author */
	private $author;

	/** @var Embed\Field[] */
	private $fields = [];

	public function getTitle(): ?string
	{
		return $this->title;
	}

	public function getDescription(): ?string
	{
		return $this->description;
	}

	public function getUrl(): ?string
	{
		return $this->url;
	}

	public function getColor(): ?int
	{
		return $this->color;
	}

	public function getTimestamp(): ?DateTimeInterface
	{
		return $this->timestamp;
	}

	public function getFooter(): ?Embed\Footer
	{
		return $this->footer;
	}

	public function getThumbnail(): ?Embed\Image
	{
		return $this->thumbnail;
	}

	public function getImage(): ?Embed\Image
	{
		return $this->image;
	}

	public function getAuthor(): ?Embed\Author
	{
		return $this->author;
	}

	public function setTitle(?string $title): void
	{
		$this->title = $title;
	}

	public function setDescription(?string $description): void
	{
		$this->description = $description;
	}

	public function setUrl(?string $url): void
	{
		$this->url = $url;
	}

	public function setColor(?int $color): void
	{
		$this->color = $color;
	}

	public function setTimestamp(?DateTimeInterface $timestamp): void
	{
		$this->timestamp = $timestamp;
	}

	public function setFooter(?Embed\Footer $footer): void
	{
		$this->footer = $footer;
	}

	public function setThumbnail(?Embed\Image $thumbnail): void
	{
		$this->thumbnail = $thumbnail;
	}

	public function setImage(?Embed\Image $image): void
	{
		$this->image = $image;
	}

	public function setAuthor(?Embed\Author $author): void
	{
		$this->author = $author;
	}

	public function &getFields(): array
	{
		return $this->fields;
	}

}