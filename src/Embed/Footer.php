<?php

declare(strict_types=1);
namespace Adawolfa\DiscordHooks\Embed;
use Nette\SmartObject;
use JsonSerializable;
use Adawolfa\DiscordHooks\Serializer;

/**
 * Embed footer
 *
 * @property string $iconUrl
 * @property string $text
 */
final class Footer implements JsonSerializable
{

	use SmartObject;
	use Serializer;

	/** @var string */
	private $iconUrl, $text;

	public function getIconUrl(): ?string
	{
		return $this->iconUrl;
	}

	public function getText(): string
	{
		return $this->text;
	}

	public function setIconUrl(?string $iconUrl): void
	{
		$this->iconUrl = $iconUrl;
	}

	public function setText(string $text): void
	{
		$this->text = $text;
	}

}