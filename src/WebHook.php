<?php

declare(strict_types=1);
namespace Adawolfa\DiscordHooks;
use Nette\Utils\Json;

/**
 * Discord webhook.
 */
final class WebHook
{

	/** @var string */
	private $url;

	public function __construct(string $url)
	{
		$this->url = $url;
	}

	/**
	 * Executes the webhook.
	 * @param Message $message
	 */
	public function execute(Message $message): void
	{
		$content = Json::encode($message);
		$context = stream_context_create([
			'http' => [
				'method' => 'POST',
				'header' => implode("\r\n", [
					'Content-Type: application/json',
					sprintf('Content-Length: %d', strlen($content)),
				]),
				'content' => $content,
			],
		]);

		@file_get_contents($this->url, false, $context);
	}

}