<?php

declare(strict_types=1);
namespace Adawolfa\DiscordHooks\Embed;
use Nette\SmartObject;
use JsonSerializable;
use Adawolfa\DiscordHooks\Serializer;

/**
 * Embed author.
 *
 * @property string $name
 * @property string $url
 * @property string $iconUrl
 */
final class Author implements JsonSerializable
{

	use SmartObject;
	use Serializer;

	/** @var string */
	private $name, $url, $iconUrl;

	public function __construct(string $name)
	{
		$this->setName($name);
	}

	public function getName(): string
	{
		return $this->name;
	}

	public function getUrl(): ?string
	{
		return $this->url;
	}

	public function getIconUrl(): ?string
	{
		return $this->iconUrl;
	}

	public function setName(string $name): void
	{
		$this->name = $name;
	}

	public function setUrl(?string $url): void
	{
		$this->url = $url;
	}

	public function setIconUrl(?string $iconUrl): void
	{
		$this->iconUrl = $iconUrl;
	}

}