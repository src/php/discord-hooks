<?php

declare(strict_types=1);
namespace Adawolfa\DiscordHooks\React;
use Adawolfa\DiscordHooks;
use Nette\Utils\Json;
use React;

/**
 * Discord webhook for ReactPHP.
 */
final class WebHook
{

	/** @var React\HttpClient\Client */
	private $http;

	/** @var string */
	private $url;

	public function __construct(React\EventLoop\LoopInterface $loop, string $url)
	{
		$this->url = $url;
		$this->http = new React\HttpClient\Client($loop);
	}

	/**
	 * Executes the webhook.
	 * @param DiscordHooks\Message $message
	 */
	public function execute(DiscordHooks\Message $message): void
	{
		$content = Json::encode($message);

		$request = $this->http->request('POST', $this->url, [
			'Content-Type' => 'application/json',
			'Content-Length' => strlen($content),
		]);

		$request->end($content);
	}

}