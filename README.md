# Discord hooks

## Installation

~~~bash
cd opt
composer create-project adawolfa/discord-hooks
~~~

## Setup Git hook

~~~bash
ln -s /opt/discord-hooks/git/post-receive repository.git/hooks/post-receive
~~~

In the repository directory, create configuration file `discord.ini`:

~~~ini
; WebHook URL
webhook    = https://discordapp.com/api/webhooks/...

; Commit web interface URL (optional)
url_commit = https://.../%dir%/commit/%sha%

; Author profile URL (optional)
url_author = https://.../%name%/%email%

; Branch URL
url_ref = https://.../%dir%/branch/%ref%

; Branch filter (optional)
branch     = master
~~~